package com.jutt.medsreminders.utils

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.os.bundleOf
import com.jutt.medsreminders.R
import java.util.*

interface NotificationManagerProvider {
    fun getNotificationManager(context: Context): NotificationManager {
        return context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }
}

interface NotificationChannelCreator : NotificationManagerProvider {

    fun createNotificationChannel(context: Context): String

    fun createNotificationsChannel(
        context: Context,
        channelId: String,
        channelName: String,
        importance: Int,
        isVibrationEnabled: Boolean = true,
        isLightEnabled: Boolean = true
    ): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the channel for the notification
            val channel = NotificationChannel(
                channelId,
                channelName,
                importance
            ).apply {
                enableVibration(isVibrationEnabled)
                enableLights(isLightEnabled)
            }

            // Set the Notification Channel for the Notification Manager.
            getNotificationManager(context).createNotificationChannel(channel)

            channelId
        } else DEFAULT_CHANNEL
    }

    companion object {
        const val DEFAULT_CHANNEL = "default"
    }

    object AlertsChannel : NotificationChannelCreator {
        private const val CHANNEL_ID = "MedicineEat"

        @TargetApi(Build.VERSION_CODES.O)
        override fun createNotificationChannel(context: Context) =
            createNotificationsChannel(
                context,
                CHANNEL_ID,
                context.getString(R.string.app_name),
                NotificationManager.IMPORTANCE_HIGH
            )
    }

}

interface NotificationHandler : NotificationManagerProvider {
    fun createNotification(
        context: Context,
        channelId: String,
        title: CharSequence? = null,
        message: CharSequence? = null,
        extras: Bundle? = null
    ): Notification

    fun addNotification(context: Context, id: Int, notification: Notification) {
        getNotificationManager(context).notify(id, notification)
    }

    fun removeNotification(context: Context, id: Int) {
        getNotificationManager(context).cancel(id)
    }

    object SimpleNotification : NotificationHandler {
        override fun createNotification(
            context: Context,
            channelId: String,
            title: CharSequence?,
            message: CharSequence?,
            extras: Bundle?
        ): Notification {
            return with(NotificationCompat.Builder(context, channelId)) {
                priority = NotificationCompat.PRIORITY_HIGH
                setSmallIcon(R.mipmap.ic_launcher)

                if (title?.isNotEmpty() == true) {
                    val spannableTitle = SpannableString(title).apply {
                        setSpan(
                            StyleSpan(Typeface.BOLD),
                            0,
                            title.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    }
                    setContentTitle(spannableTitle)
                }

                if (message?.isNotEmpty() == true) {
                    setContentText(message)
                }

                return@with build()
            }
        }
    }

    object AlertNotification : NotificationHandler {

        const val KEY_NAVIGATION_ID = "navigationId"

        private const val DEEP_LINK_URI = "app://medicineeat/home"
        private const val REQUEST_CODE = 1001

        private const val DEEP_LINK_ENABLED = "isDeepLinkEnabled"

        fun createExtras(isDeepLinkEnabled: Boolean) = bundleOf(
            DEEP_LINK_ENABLED to isDeepLinkEnabled
        )

        override fun createNotification(
            context: Context,
            channelId: String,
            title: CharSequence?,
            message: CharSequence?,
            extras: Bundle?
        ): Notification {
            // Get the layouts to use in the custom notification
            val notificationLayout =
                RemoteViews(context.packageName, R.layout.layout_alert_notification)

            val expandedNotificationLayout =
                RemoteViews(context.packageName, R.layout.layout_alert_notification_expanded)

            val formattedTime = getFormattedTime()
            bindLayout(notificationLayout, title, message, " \u2022 $formattedTime")
            bindLayout(expandedNotificationLayout, title, message, formattedTime)

            val pendingIntent = if (extras?.getBoolean(DEEP_LINK_ENABLED) == true) {
                val intent = createIntent()
                createPendingIntent(context, intent)
            } else null

            // Apply the layouts to the notification
            return NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCustomContentView(notificationLayout)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setCustomBigContentView(expandedNotificationLayout)
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                .build()
        }

        private fun createIntent() = Intent().apply {
            action = Intent.ACTION_VIEW
            data = Uri.parse(DEEP_LINK_URI)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }.apply {
            putExtra(KEY_NAVIGATION_ID, 100)
        }

        private fun createPendingIntent(context: Context, intent: Intent) =
            PendingIntent.getActivity(
                context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        private fun bindLayout(
            view: RemoteViews,
            title: CharSequence?,
            message: CharSequence?,
            time: String?
        ) {
            view.setTextViewText(R.id.tv_title, title)
            view.setTextViewText(R.id.tv_time, time)
            view.setTextViewText(R.id.tv_description, message)
        }

        private fun getFormattedTime(): String {
            val currentTime = Calendar.getInstance().time
            return DateTimeUtils.getFormattedNotificationTime(currentTime)
        }
    }

}