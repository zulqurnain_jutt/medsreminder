package com.jutt.medsreminders.utils

import android.content.Context
import com.jutt.medsreminders.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object DateTimeUtils {

    data class Breakdown(
        val days: Long,
        val hours: Long,
        val minutes: Long,
        val seconds: Long,
        val milliseconds: Long
    ) {
        val durationInHoursRounded: Long get() = ((days * 24) + hours) + if (minutes > 30) 1 else 0

        val durationInHours: Long
            get() = (days * HOURS_IN_DAY) + hours

        val durationInMinutes: Long
            get() = (durationInHours * MINUTES_IN_HOUR) + minutes

        val durationInSeconds: Long
            get() = (durationInMinutes * SECONDS_IN_MINUTE) + seconds
    }

    private const val HOURS_IN_DAY = 24
    private const val MINUTES_IN_HOUR = 60
    private const val SECONDS_IN_MINUTE = 60

    const val ONE_SECOND: Long = 1000
    const val ONE_MINUTE: Long = 60 * ONE_SECOND
    const val ONE_HOUR: Long = 60 * ONE_MINUTE
    const val ONE_DAY: Long = 24 * ONE_HOUR
    const val SERVER_DATE_FORMAT: String = "yyyy-MM-dd"
    const val NOTIFICATION_TIME_FORMAT: String = "dd MMM hh:mm a"
    const val ORDER_TIME = "dd MMM, yyyy hh:mm a"

    private val serverDateFormatter: SimpleDateFormat by lazy {
        SimpleDateFormat(
            SERVER_DATE_FORMAT,
            Locale.ENGLISH
        )
    }

    private val notificationTimeFormatter: SimpleDateFormat by lazy {
        SimpleDateFormat(
            NOTIFICATION_TIME_FORMAT,
            Locale.ENGLISH
        )
    }

    private val orderTimeFormatter: SimpleDateFormat by lazy {
        SimpleDateFormat(
            ORDER_TIME,
            Locale.ENGLISH
        )
    }

    fun secondsBreakdown(seconds: Long): Breakdown =
        timeDifferenceBreakdown(difference = TimeUnit.SECONDS.toMillis(seconds))

    fun timeDifferenceBreakdown(difference: Long): Breakdown {
        var millis = difference
        val days = millis / ONE_DAY
        millis %= ONE_DAY
        val hours = millis / ONE_HOUR
        millis %= ONE_HOUR
        val minutes = millis / ONE_MINUTE
        millis %= ONE_MINUTE
        val seconds = millis / ONE_SECOND
        return Breakdown(days, hours, minutes, seconds, millis)
    }

    fun timeDifferenceBreakdown(
        startTimeInMillis: Long = Calendar.getInstance().timeInMillis,
        endTimeInMillis: Long
    ): Breakdown {
        return timeDifferenceBreakdown(difference = (endTimeInMillis - startTimeInMillis))
    }

    fun getFormattedNotificationTime(date: Date): String =
        notificationTimeFormatter.format(date)

    fun getFormattedOrderTime(date: Date): String = orderTimeFormatter.format(date)
    
    fun isDateSame(lhs: Calendar, rhs: Calendar): Boolean =
        lhs.get(Calendar.YEAR) == rhs.get(Calendar.YEAR)
                && lhs.get(Calendar.DAY_OF_YEAR) == rhs.get(Calendar.DAY_OF_YEAR)


    fun isDateToday(calendar: Calendar): Boolean =
        isDateSame(lhs = calendar, rhs = Calendar.getInstance())

    fun isMonthSame(lhs: Calendar, rhs: Calendar): Boolean =
        lhs.get(Calendar.YEAR) == rhs.get(Calendar.YEAR)
                && lhs.get(Calendar.MONTH) == rhs.get(Calendar.MONTH)


    fun now() = Calendar.getInstance().time

    fun fromSeconds(secondsTimeStamp: Long) = Date(secondsTimeStamp * 1000)

    fun fromSeconds(secondsTimeStamp: Long?) = secondsTimeStamp?.let {
        Date(it * 1000)
    }

    fun toSeconds(milliseconds: Long) = milliseconds * 1000

}

object DateFormats {
    const val DAY_MONTH = "dd MMM"
    const val DAY_MONTH_YEAR = "dd MMM yyyy"
    const val DAY_ONLY = "dd"
    const val MONTH_YEAR = "MMM yyyy"
    const val SHIFT_DATE = "EEEE, dd MMM yyyy"
}

object TimeFormats {
    const val HOUR_MINUTE_AM = "hh:mm a"
}

fun Date.formatAs(format: String): String {
    val simpleDateFormat = SimpleDateFormat(format, Locale.ENGLISH)
    return simpleDateFormat.format(this)
}

fun Date.toCalendar(): Calendar = Calendar.getInstance().apply {
    this.time = this@toCalendar
}

fun Date.isSameDate(cmp: Date) =
    DateTimeUtils.isDateSame(this.toCalendar(), cmp.toCalendar())

fun Date.isNotSameDate(cmp: Date) = !this.isSameDate(cmp)
