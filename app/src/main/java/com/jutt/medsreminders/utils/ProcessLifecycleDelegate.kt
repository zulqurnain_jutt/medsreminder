package com.jutt.medsreminders.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ProcessLifecycleOwner

interface ProcessLifecycleDelegate {

    private fun getLifeCycle() = ProcessLifecycleOwner.get().lifecycle

    fun getCurrentState() = getLifeCycle().currentState

    fun isInForeground() = getCurrentState() == Lifecycle.State.RESUMED
}