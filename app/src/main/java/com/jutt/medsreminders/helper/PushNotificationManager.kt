package com.jutt.medsreminders.helper

import android.content.Context
import com.google.gson.Gson
import com.jutt.medsreminders.data.bo.PushNotificationData
import com.jutt.medsreminders.data.enums.PushNotificationAction
import com.jutt.medsreminders.data.models.Alert
import com.jutt.medsreminders.injection.Injector
import com.jutt.medsreminders.utils.NotificationChannelCreator
import com.jutt.medsreminders.utils.NotificationHandler
import com.jutt.medsreminders.utils.ProcessLifecycleDelegate
import kotlinx.coroutines.CoroutineDispatcher
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named
import kotlin.random.Random

class PushNotificationManager @Inject constructor(
    private val context: Context,
    private val eventTracker: EventTracker,
    @Named(Injector.Named.REPOSITORY_DISPATCHER)
    private val dispatcher: CoroutineDispatcher
) : ProcessLifecycleDelegate {

    fun onNotificationReceived(payload: Map<String, String>?): Boolean {
        try {
            val json: String? = payload?.get("data")
            val additionalData = JSONObject(json ?: return false)
            val pushNotificationData = parseNotificationData(data = additionalData)
            when (pushNotificationData.action) {
                PushNotificationAction.ALERT -> {
                    val data = pushNotificationData.data as? Alert ?: return false
                    handleAlertNotification(context, data)
                    return true
                }

                else -> eventTracker.logEvent(EventTracker.Events.OTHER_NOTIFICATIONS, null)
            }
            return false
        } catch (ex: Exception) {
            Timber.e(ex, "Error in parsing notification data")
        }

        return false
    }

    private fun handleAlertNotification(context: Context, data: Alert) = handleAlertNotification(
        context = context,
        id = Random.nextInt(),
        title = data.title,
        message = data.description,
        isDeepLinkEnabled = true
    )

    private fun handleAlertNotification(
        context: Context,
        id: Int,
        title: CharSequence,
        message: CharSequence,
        isDeepLinkEnabled: Boolean
    ) {
        val channelCreator =
            object : NotificationChannelCreator by NotificationChannelCreator.AlertsChannel {}

        val simpleNotification =
            object : NotificationHandler by NotificationHandler.AlertNotification {}

        val channel = channelCreator.createNotificationChannel(context)

        val notification = simpleNotification.createNotification(
            context, channel,
            title = title,
            message = message,
            extras = NotificationHandler.AlertNotification.createExtras(isDeepLinkEnabled)
        )

        simpleNotification.addNotification(context, id, notification)
    }

    @Throws(Exception::class)
    private fun parseNotificationData(data: JSONObject): PushNotificationData<Any?> {
        val action = PushNotificationAction.fromValue(value = data.optInt("action", -1))
        val gson = Gson()
        return PushNotificationData(
            action = action,
            alert = data.optString("alert") ?: "",
            data = when (action) {
                PushNotificationAction.ALERT -> gson.fromJson(
                    data.optString("alert_data"),
                    Alert::class.java
                )
                else -> null
            }
        )
    }
}
