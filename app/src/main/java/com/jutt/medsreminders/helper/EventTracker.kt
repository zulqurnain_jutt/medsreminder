package com.jutt.medsreminders.helper

import android.app.Application
import android.os.Bundle
import androidx.annotation.Size
import com.google.firebase.analytics.FirebaseAnalytics
import com.jutt.medsreminders.application.MyApp
import com.jutt.medsreminders.injection.Injector
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class EventTracker @Inject constructor(
    private val analytics: FirebaseAnalytics,
    @Named(Injector.Named.LOGGING_ENABLED)
    private val isLoggingEnabled: Boolean
) {

    companion object {
        fun getInstance(application: Application): EventTracker? {
            return (application as? MyApp)?.injector?.provide(EventTracker::class.java)
        }
    }

    object Events {
        const val OTHER_NOTIFICATIONS: String = "other_notifications"
    }

    fun logEvent(@Size(min = 1L, max = 40L) key: String, extras: Bundle?) {
        if (isLoggingEnabled) {
            try {
                analytics.logEvent(key, extras)
            } catch (ex: Exception) {
                Timber.e(ex, "Error pushing event: $key")
            }
        }
    }

}
