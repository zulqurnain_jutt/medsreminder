package com.jutt.medsreminders.workmanagers

import android.content.Context
import androidx.work.*
import java.util.concurrent.TimeUnit

/**
 *
 */
interface WorkManagerDelegate : ConstraintBuilder, RequestBuilder {

    private fun createPeriodicScheduledRequest(
        constraints: Constraints?,
        inputData: Data?,
        delayInSeconds: Long?,
        tag: String?
    ) = createPeriodicRequest().apply {
        constraints?.let {
            setConstraints(it)
        }

        inputData?.let {
            setInputData(it)
        }

        delayInSeconds?.let {
            setInitialDelay(it, TimeUnit.SECONDS)
        }

        tag?.let {
            addTag(it)
        }
    }.build()
    private fun createScheduledRequest(
        constraints: Constraints?,
        inputData: Data?,
        delayInSeconds: Long?,
        tag: String?
    ) = createRequest().apply {
        constraints?.let {
            setConstraints(it)
        }

        inputData?.let {
            setInputData(it)
        }

        delayInSeconds?.let {
            setInitialDelay(it, TimeUnit.SECONDS)
        }

        tag?.let {
            addTag(it)
        }
    }.build()

    fun scheduleOnTimeRequest(
        context: Context,
        tag: String,
        inputData: Data? = null,
        delayInSeconds: Long? = 0L,
        workPolicy: ExistingWorkPolicy = ExistingWorkPolicy.REPLACE
    ): Operation {
        val constraints = createConstraints()

        val job = createScheduledRequest(constraints, inputData, delayInSeconds, tag)

        return WorkManager.getInstance(context)
            .beginUniqueWork(tag, workPolicy, job)
            .enqueue()
    }
    fun scheduleDailyRequest(
        context: Context,
        tag: String,
        inputData: Data? = null,
        delayInSeconds: Long? = 0L,
        workPolicy: ExistingPeriodicWorkPolicy = ExistingPeriodicWorkPolicy.KEEP
    ): Operation {
        val constraints = createConstraints()

        val job = createPeriodicScheduledRequest(constraints, inputData, delayInSeconds, tag)

        return WorkManager.getInstance(context)
            .enqueueUniquePeriodicWork(tag, workPolicy, job)
    }
}

/**
 *
 */
interface ConstraintBuilder {
    fun createConstraints(): Constraints?

    object NoConstraints : ConstraintBuilder {
        override fun createConstraints(): Constraints? = null
    }

    object NetworkConstraints : ConstraintBuilder {
        override fun createConstraints() = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
    }
}

/**
 *
 */
interface RequestBuilder {
    fun createPeriodicRequest(): PeriodicWorkRequest.Builder
    fun createRequest(): OneTimeWorkRequest.Builder
}