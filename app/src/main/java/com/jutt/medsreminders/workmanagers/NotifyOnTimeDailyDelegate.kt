package com.jutt.medsreminders.workmanagers

import android.content.Context
import androidx.work.*
import com.blankj.utilcode.constant.TimeConstants
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.TimeUtils
import com.jutt.medsreminders.helper.PushNotificationManager
import com.google.gson.Gson
import com.jutt.medsreminders.application.MyApp
import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.data.models.NotificationAlert
import com.jutt.medsreminders.data.repositories.DatabaseRepository
import com.jutt.medsreminders.injection.Injector
import java.util.concurrent.TimeUnit


interface NotifyOnTimeDailyDelegate {
    fun scheduleOneTimeRequest(
        context: Context,
        initialDelay: Long = DELAY_IN_SECONDS
    ) {
        val worker = object : WorkManagerDelegate,
            ConstraintBuilder by ConstraintBuilder.NetworkConstraints,
            RequestBuilder by NotifyOnTimeDailyRequestBuilder {}

        worker.scheduleOnTimeRequest(
            context = context,
            tag = TAG,
            delayInSeconds = initialDelay
        )
    }

    fun scheduleDailyRequest(
        context: Context,
        delay: Long = TimeUtils.getDateByNow(1, TimeConstants.DAY).time,
//        medicineTake: MedicineTake?
    ) {
        val worker = object : WorkManagerDelegate,
            ConstraintBuilder by ConstraintBuilder.NoConstraints,
            RequestBuilder by NotifyOnTimeDailyRequestBuilder {}

//        val inputData = medicineTake?.let {
//            createInputData(medicineTake)
//        }?: kotlin.run { null }

        worker.scheduleDailyRequest(
            context = context,
            tag = TAG,
            delayInSeconds = delay,
//            inputData = inputData
        )
    }

    fun callTasks(context: Context) {
        WorkManager.getInstance(context).cancelAllWorkByTag(TAG)
    }

//    private fun createInputData(medicineTake: MedicineTake) = Data.Builder()
//        .putString(KEY_MEDICINE_OBJECT, GsonUtils.toJson(medicineTake))
//        .build()

    companion object {
        const val TAG = "NotifyOnTimeDaily"
        private const val DELAY_IN_SECONDS = 1L

//        const val KEY_MEDICINE_OBJECT = "medicineObject"
    }
}

/**
 * Pushes notification on Specific time every day
 */
class NotifyOnTimeDailyWorker(appContext: Context, workerParams: WorkerParameters) :
    CoroutineWorker(appContext, workerParams),NotifyOnTimeDailyDelegate {

    private val injector: Injector? by lazy { (applicationContext as? MyApp)?.injector }

    private val pushNotificationManager: PushNotificationManager? by lazy {
        injector?.provide(PushNotificationManager::class.java)
    }

    private val dbRepo: DatabaseRepository? by lazy {
        injector?.provide(DatabaseRepository::class.java)
    }

    override suspend fun doWork(): Result {
//        val medicineTake:MedicineTake? = inputData.getString(KEY_MEDICINE_OBJECT)?.convertTo()
        val medicineTake:MedicineTake? = dbRepo?.medicineReminderDao()?.getSelected()

        medicineTake?.let {
            sendLocalNotificationForMeds(medicineTake)

            // schedule next
            scheduleOneTimeRequest(
                context = applicationContext,
                initialDelay = medicineTake.timeDiffMedInSeconds
            )
        }
        return Result.success()
    }

    private fun sendLocalNotificationForMeds(item: MedicineTake) {
        val notificationAlert = NotificationAlert(
            data = NotificationAlert.Data(
                action = 1,
                alert = item.medicineDescription,
                alertData = NotificationAlert.Data.AlertData(
                    id = item.id,
                    title = item.daySlot?.value ?: "",
                    description = item.medicineDescription,
                    isSeen = false,
                    updatedAt = TimeUtils.getNowDate().time
                )
            )
        )

        val mapOfValues: MutableMap<String, Any> = Gson().fromJson(
            GsonUtils.toJson(notificationAlert), GsonUtils.getMapType(String::class.java,Any::class.java)
        )
        val mapOfString: Map<String,String> = mapOfValues.map { it.key to it.value }.toTypedArray()
            .associateBy (
                keySelector = {it.first},
                valueTransform = {Gson().toJson(it.second)}
            )

        pushNotificationManager?.onNotificationReceived(mapOfString)
    }
}

/**
 * Request Builder
 *
 * Creates a one time request builder for #NotifyOnTimeDailyRequestBuilder
 */
object NotifyOnTimeDailyRequestBuilder : RequestBuilder {
    override fun createRequest(): OneTimeWorkRequest.Builder {
        return OneTimeWorkRequestBuilder<NotifyOnTimeDailyWorker>()
    }

    override fun createPeriodicRequest(): PeriodicWorkRequest.Builder {
        return PeriodicWorkRequest.Builder(
            NotifyOnTimeDailyWorker::class.java,
            1,
            TimeUnit.MINUTES
        ).addTag(NotifyOnTimeDailyDelegate.TAG)
    }
}

