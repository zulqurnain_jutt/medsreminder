package com.jutt.medsreminders.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jutt.medsreminders.architecture.Event
import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.data.repositories.DatabaseRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val application: Context,
    private val database: DatabaseRepository,
) : ViewModel() {

    object Events {
        const val NAVIGATE_TO_HOME: String = "NAVIGATE_TO_HOME"
    }

    private val _showLoader = MutableLiveData<Boolean>()
    val showLoader: LiveData<Boolean> get() = _showLoader

    private val _navigate = MutableLiveData<Event<String>>()
    val navigate: LiveData<Event<String>> get() = _navigate

    private val _successMessage = MutableLiveData<Event<String>>()
    val successMessage: LiveData<Event<String>> get() = _successMessage

    private var _medicineReminders = MutableLiveData<List<MedicineTake>>()
    val medicineReminders: LiveData<List<MedicineTake>> get() = _medicineReminders

    private val _toolbarVisible = MutableLiveData<Boolean>()
    val toolbarVisible: LiveData<Boolean> get() = _toolbarVisible

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    fun start() {
        navigateToHome()
    }

    fun navigateToHome() {
        navigateToSection(Events.NAVIGATE_TO_HOME)
    }

    fun navigateToSection(eventContent: String) {
        _navigate.value = Event.create(content = eventContent)
    }

    fun getMedicineReminders() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _showLoader.postValue(true)
                _medicineReminders.postValue(database.medicineReminderDao().loadAll())
                _showLoader.postValue(false)
            }
        }
    }

    fun addifNotMedicineReminder(medicineTake: MedicineTake) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _showLoader.postValue(true)
                try {
                    database.medicineReminderDao().insertIfNotExist(medicineTake)
                } catch (e: Exception) {
                }
                getMedicineReminders()
                _showLoader.postValue(false)
            }
        }
    }

    fun selectMedicineNotify(id: Long) {
        viewModelScope.launch{
            withContext(Dispatchers.IO) {
                _showLoader.postValue(true)
                database.medicineReminderDao().unSelectAll()
                database.medicineReminderDao().singleSelect(id)

                _medicineReminders.postValue(database.medicineReminderDao().loadAll()) // TODO >>REMOVE THIS LINE<< added this to just show off my progress pop up
                delay(TimeUnit.SECONDS.toMillis(1)) // TODO >>REMOVE THIS LINE<< added this to just show off my progress pop up

                getMedicineReminders()
                _showLoader.postValue(false)
            }
        }
    }

    fun setToolbarVisibility(visible: Boolean) {
        _toolbarVisible.postValue(visible)
    }
}
