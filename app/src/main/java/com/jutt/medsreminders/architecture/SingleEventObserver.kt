package com.jutt.medsreminders.architecture


import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Observer that receives only one update after subscription, used for one time events.
 *
 * This avoids a common problem with events: on network response, an update can be listened if the
 * observer is active. This Observer only calls the registerObserver for the first time and
 * unregister itself before it emits the value that has been received.
 *
 * Note that only one value is going to be delivered from this observer.
 */
abstract class SingleEventObserver<T>(private val source: LiveData<T>) : Observer<T> {

    /**
     * Called when the dealsData is changed.
     *
     * @param data  The new dealsData delivered
     */
    abstract fun onChangeReceived(data: T?)

    private val pending = AtomicBoolean(false)

    override fun onChanged(t: T?) {
        if (pending.compareAndSet(false, true)) {
            removeObserver()
            onChangeReceived(t)
        }
    }

    private fun removeObserver() {
        source.removeObserver(this)
    }

}
