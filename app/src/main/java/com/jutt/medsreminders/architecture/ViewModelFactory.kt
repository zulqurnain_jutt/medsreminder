package com.jutt.medsreminders.architecture

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jutt.medsreminders.application.MyApp

/**
 * ViewModel factory to create ViewModel objects by creating their instances and completing
 * dependency requirements
 */
class ViewModelFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        (context.applicationContext as MyApp).injector?.provide(modelClass)
            ?: throw IllegalStateException("Injector not initialized")

}
