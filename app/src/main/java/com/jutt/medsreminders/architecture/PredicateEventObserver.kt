package com.jutt.medsreminders.architecture


import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

abstract class PredicateEventObserver<T>(
    private val source: LiveData<T>,
    private val predicate: (T?) -> Boolean
) : Observer<T> {

    /**
     * Called when the data is changed.
     *
     * @param data  The new data delivered
     */
    abstract fun onChangeReceived(data: T?)

    private val pending = AtomicBoolean(false)

    override fun onChanged(value: T?) {
        val emit: Boolean = predicate(value)
        if (emit && pending.compareAndSet(false, true)) {
            removeObserver()
            onChangeReceived(value)
        }
    }

    private fun removeObserver() {
        source.removeObserver(this)
    }

}
