package com.jutt.medsreminders.data.persistence.daos

import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jutt.medsreminders.data.models.MedicineTake

@Dao
interface MedicineRemindersDao : BaseDao<MedicineTake> {
    @Query("SELECT * FROM MedicineTake WHERE id = :id LIMIT 1")
    fun loadById(id: Long): MedicineTake

    @Query("SELECT * FROM MedicineTake WHERE isSelected = 1")
    fun getSelected(): MedicineTake

    @Query("DELETE FROM MedicineTake WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("SELECT * FROM MedicineTake")
    fun loadAll(): List<MedicineTake>

    @Query("UPDATE MedicineTake SET isSelected = 1 WHERE id = :id")
    fun singleSelect(id: Long): Int

    @Insert
    fun insertIfNotExist(vararg items: MedicineTake)

    @Query("UPDATE MedicineTake SET isSelected = 0")
    fun unSelectAll(): Int

    @Query("DELETE FROM MedicineTake")
    fun clearTable()
}
