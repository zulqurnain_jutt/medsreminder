package com.jutt.medsreminders.data.models

import com.google.gson.annotations.SerializedName
import com.jutt.medsreminders.extensions.timeInSeconds
import java.util.*

data class Alert(
    @SerializedName("id") val id: Long = 0,
    @SerializedName("title") val title: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("updated_at") val updatedAt: Long = 0,
    @SerializedName("is_seen") var isSeen: Boolean = true
) {
    val updateAtCalendar: Calendar
        get() = Calendar.getInstance().also {
            it.timeInSeconds = updatedAt
        }
}
