package com.jutt.medsreminders.data.persistence

import androidx.room.TypeConverter
import com.jutt.medsreminders.data.models.DaySlot

class Converters {

    @TypeConverter
    fun daySlotToValue(daySlot: DaySlot): Int = daySlot.ordinal

    @TypeConverter
    fun daySlotFromValue(ordinal: Int): DaySlot = DaySlot.values()[ordinal]
}
