package com.jutt.medsreminders.data.models


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.blankj.utilcode.constant.TimeConstants
import com.blankj.utilcode.util.TimeUtils
import com.google.gson.annotations.SerializedName
import com.jutt.medsreminders.extensions.plusSeconds
import com.jutt.medsreminders.extensions.to12HourTimeString
import kotlinx.android.parcel.Parcelize
import java.util.*
import java.util.Calendar.*
import java.util.concurrent.TimeUnit

@Entity
@Parcelize
data class MedicineTake(
    @SerializedName("id") @PrimaryKey val id: Long = 0,
    @SerializedName("medicine_description") val medicineDescription: String = "",
    @SerializedName("day_slot") val daySlot: DaySlot? = DaySlot.MORNING,
    @SerializedName("time_for_medicine") var timeOfMed: String = "8:00 AM",
    @SerializedName("is_selected") var isSelected: Boolean = false
) : Parcelable {
    val timeDiffMedInSeconds: Long
    get() {
        /////////// TODO >>REMOVE This<<< just testing
        if(daySlot == DaySlot.UNKNOWN){
            timeOfMed = Date().plusSeconds(60).to12HourTimeString()
        }
        ////////////////////////////////////////

        val medTimeCalender = getInstance().apply {
            time = TimeUtils.getDate(
                timeOfMed, TimeUtils.getSafeDateFormat("hh:mm a"), 0L, TimeConstants.SEC
            )
        }

        val myTime = getInstance().apply {
            time = TimeUtils.getNowDate()
            this[HOUR_OF_DAY] = medTimeCalender[HOUR_OF_DAY]
            this[MINUTE] = medTimeCalender[MINUTE]
            this[SECOND] = medTimeCalender[SECOND]
        }
        if (myTime.timeInMillis - TimeUtils.getNowDate().time < 0) {
            // time have passed on current day
            myTime[DAY_OF_MONTH] = myTime[DAY_OF_MONTH] + 1
        }
        return TimeUnit.MILLISECONDS.toSeconds(myTime.timeInMillis - TimeUtils.getNowDate().time)
    }
}