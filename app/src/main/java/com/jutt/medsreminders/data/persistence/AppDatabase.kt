
package com.jutt.medsreminders.data.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.data.persistence.daos.MedicineRemindersDao

@Database(
    version = 1,
    entities = [
        MedicineTake::class
    ]
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun medicineReminderDao(): MedicineRemindersDao

    companion object {
        private const val DATABASE_NAME = "medicine-reminders-db"

        fun buildDatabase(
            context: Context,
        ): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .addCallback(object : Callback() {
                override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
                    super.onDestructiveMigration(db)
//                    preferences.clearAuthUser()
                }
            })
            .build()
    }
}

