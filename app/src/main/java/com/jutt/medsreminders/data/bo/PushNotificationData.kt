package com.jutt.medsreminders.data.bo

import com.jutt.medsreminders.data.enums.PushNotificationAction

data class PushNotificationData<T>(
    val action: PushNotificationAction = PushNotificationAction.UNKNOWN,
    val alert: String = "",
    val data: T? = null
)
