package com.jutt.medsreminders.data.models


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class NotificationAlert(
    @SerializedName("data")
    val `data`: Data? = null
) : Parcelable {
    @Parcelize
    data class Data(
        @SerializedName("action")
        val action: Long? = null, // 1
        @SerializedName("alert")
        val alert: String? = null, // some str
        @SerializedName("alert_data")
        val alertData: AlertData? = null
    ) : Parcelable {
        @Parcelize
        data class AlertData(
            @SerializedName("description")
            val description: String? = null,
            @SerializedName("id")
            val id: Long? = null, // 1
            @SerializedName("is_seen")
            val isSeen: Boolean? = null, // false
            @SerializedName("title")
            val title: String? = null,
            @SerializedName("updated_at")
            val updatedAt: Long? = null
        ) : Parcelable
    }
}