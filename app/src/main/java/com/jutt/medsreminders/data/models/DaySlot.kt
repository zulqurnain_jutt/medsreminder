package com.jutt.medsreminders.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

enum class DaySlot(val value: String) {

    @SerializedName(value = "Unknown")
    UNKNOWN(value = "Unknown"),
    @SerializedName(value = "Morning")
    MORNING(value = "Morning"),
    @SerializedName(value = "Afternoon")
    AFTERNOON(value = "Afternoon"),
    @SerializedName(value = "Night")
    NIGHT(value = "Night"),
    @SerializedName(value = "Evening")
    EVENING(value = "Evening");

    companion object {
        fun from(value: String?): DaySlot {
            val valueCaseInsensitive = value?.toLowerCase(Locale.ENGLISH)
            return values().find { it.value.toLowerCase(Locale.ENGLISH) == valueCaseInsensitive }
                ?: MORNING
        }
    }

}