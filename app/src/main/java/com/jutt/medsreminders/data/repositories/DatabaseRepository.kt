package com.jutt.medsreminders.data.repositories

import androidx.room.FtsOptions
import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.data.persistence.AppDatabase
import java.util.concurrent.locks.ReentrantLock
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.concurrent.withLock

@Singleton
class DatabaseRepository @Inject constructor(private val database: AppDatabase) {

    private val remindersLock: ReentrantLock = ReentrantLock()

    fun medicineReminderDao() = database.medicineReminderDao()

    fun clearAllReminders() {
        remindersLock.withLock {
            medicineReminderDao().clearTable()
        }
    }

    fun addMedicineReminder(reminder: MedicineTake){
        remindersLock.withLock {
            medicineReminderDao().insert(reminder)
        }
    }

}