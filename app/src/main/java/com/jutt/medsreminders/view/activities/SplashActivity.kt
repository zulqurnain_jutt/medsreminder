package com.jutt.medsreminders.view.activities

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import com.jutt.medsreminders.R
import com.jutt.medsreminders.core.AppSupportActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppSupportActivity() {

    private val disposables:CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        setupViews()
    }

    private fun setupViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            app_name.transitionName = getString(R.string.transition_appLogo)
        }
        val text = getString(R.string.app_name)
        val textLength = text.length

        disposables.add(
            Observable.range(0, textLength)
                .concatMap { Observable.just(it).delay(100, java.util.concurrent.TimeUnit.MILLISECONDS) }
                .map { text[it].toString() }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { char -> app_name.text = app_name.text.toString().plus(char) },
                    {e -> e.printStackTrace()}, {
                        openHome()
                    }
                )
        )
    }

    private fun openHome(){
        val intent: Intent = HomeActivity.newIntent(context = this)
        val options: ActivityOptions = ActivityOptions.makeCustomAnimation(
            this,
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            options.update(
                ActivityOptions.makeSceneTransitionAnimation(
                    this,
                    app_name,
                    app_name.transitionName
                )
            )
        }

        startActivity(intent)
        supportFinishAfterTransition()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

}
