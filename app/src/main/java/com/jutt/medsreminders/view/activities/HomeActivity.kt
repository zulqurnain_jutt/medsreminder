package com.jutt.medsreminders.view.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import com.blankj.utilcode.util.ColorUtils
import com.jutt.medsreminders.R
import com.jutt.medsreminders.architecture.*
import com.jutt.medsreminders.core.AppNavigationActivity
import com.jutt.medsreminders.extensions.*
import com.jutt.medsreminders.utils.loadImageFromDrawable
import com.jutt.medsreminders.view.fragments.*
import com.jutt.medsreminders.viewmodels.HomeViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.progress_bar
import kotlinx.android.synthetic.main.activity_home.progress_bar_iv


class HomeActivity : AppNavigationActivity(){

    private val viewModel: HomeViewModel by obtainViewModel()

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, HomeActivity::class.java)
    }

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupViews()
        setupObservers()
        setupHome()

    }


    private fun setupHome() {

        viewModel.start()
    }

    fun setupViews() {
        setupActionBar(toolbar = toolbar, action = {
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowTitleEnabled(true)
        })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.transitionName = getString(R.string.transition_appLogo)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            supportPostponeEnterTransition()
        }

        progress_bar_iv.loadImageFromDrawable(drawable = R.drawable.loading)
    }

    private fun setupObservers() {
        viewModel.toolbarVisible.observe(this,{
            toolbar.isVisible = it
        })

        viewModel.showLoader.observe(this, LoaderObserver(progressView = progress_bar))

        viewModel.successMessage.observe(this, EventObserver { message ->
            runOnUiThread {
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            }
        })

        viewModel.navigate.observe(this, NavigationObserver(listener = this))
    }

    override fun onBackPressed() {
        if (viewModel.showLoader.value != true) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }



    override fun isFragmentNavigation(event: String): Boolean {
        return when (event) {
//            HomeViewModel.Events.NAVIGATE_TO_BOOK_STORE -> false
            else -> super.isFragmentNavigation(event)
        }
    }

    override fun handleNonFragmentNavigationInstance(event: String) {
        when (event) {
//            HomeViewModel.Events.NAVIGATE_TO_TERMS_AND_CONDITIONS -> {
//                NavigationUtils.getInstance().openTermsAndCondition(this)
//            }
//            HomeViewModel.Events.NAVIGATE_TO_RATING -> {
//                startActivity(
//                )
//            }
            else -> super.isFragmentNavigation(event)
        }
    }

    override fun getFragmentNavigationInstance(event: String): NavigationInstance {
        return when (event) {
            HomeViewModel.Events.NAVIGATE_TO_HOME -> {
                NavigationInstance(
                    fragment = HomeFragment.newInstance(),
                    addToBackStack = false,
                    pushAnimation = NavigationAnimation.FADE_IN
                )
            }
            else -> super.getFragmentNavigationInstance(event)
        }
    }

}