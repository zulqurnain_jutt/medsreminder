package com.jutt.medsreminders.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.abed.kotlin_recycler.adapters.SimpleRecyclerAdapter
import com.abed.kotlin_recycler.withSimpleAdapter
import com.blankj.utilcode.util.ColorUtils
import com.blankj.utilcode.util.TimeUtils
import com.jutt.medsreminders.R
import com.jutt.medsreminders.core.AppSupportFragment
import com.jutt.medsreminders.data.models.DaySlot
import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.databinding.FragmentHomeBinding
import com.jutt.medsreminders.extensions.obtainSharedViewModel
import com.jutt.medsreminders.extensions.plusSeconds
import com.jutt.medsreminders.extensions.to12HourTimeString
import com.jutt.medsreminders.viewmodels.HomeViewModel
import com.jutt.medsreminders.workmanagers.NotifyOnTimeDailyDelegate
import kotlinx.android.synthetic.main.layout_image_card.view.*
import java.util.*
import java.util.concurrent.TimeUnit

class HomeFragment : AppSupportFragment(), NotifyOnTimeDailyDelegate {

    companion object {
        fun newInstance() = HomeFragment()
    }

    lateinit var mBinding: FragmentHomeBinding

    private val viewModel: HomeViewModel by obtainSharedViewModel()

    override val viewResourceId: Int = R.layout.fragment_home

    override val titleResId: Int
        get() = R.string.app_name

    private var remindersAdapter: SimpleRecyclerAdapter<MedicineTake>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = DataBindingUtil.inflate(inflater, viewResourceId, container, false)
        return mBinding.root.also { contentView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpObservers()
        setUpViews()

        addDummyData()
    }

    private fun setUpObservers() {
        viewModel.medicineReminders.observe(viewLifecycleOwner, { list ->
            remindersAdapter = mBinding.rvReminders.withSimpleAdapter(
                dataList = list,
                R.layout.layout_image_card
            ) { data ->
                bindReminders(itemView,data)
            }
        })
    }

    private fun bindReminders(itemView: View,data: MedicineTake) {
        itemView.mtv_day_slot_name.text = data.daySlot?.value
        itemView.root_view.isSelected = data.isSelected
        if(data.isSelected){
            itemView.mtv_day_slot_name.setTextColor(ColorUtils.getColor(R.color.gray1))
            itemView.root_view.setCardBackgroundColor(ColorUtils.getColor(R.color.colorPrimary))
            itemView.image_pill.setColorFilter(
                ColorUtils.getColor(R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            itemView.image_block.setColorFilter(
                ColorUtils.getColor(R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        }else{
            itemView.mtv_day_slot_name.setTextColor(ColorUtils.getColor(R.color.colorPrimary))
            itemView.root_view.setCardBackgroundColor(ColorUtils.getColor(R.color.gray1))
            itemView.image_pill.setColorFilter(
                ColorUtils.getColor(R.color.primaryTextColor),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            itemView.image_block.setColorFilter(
                ColorUtils.getColor(R.color.primaryTextColor),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        }

        itemView.root_view.setCardBackgroundColor(
            if (data.isSelected) ColorUtils.getColor(R.color.colorPrimary)
            else ColorUtils.getColor(R.color.gray1)
        )
        itemView.root_view.setOnClickListener {
            data.isSelected = data.isSelected.not()

            if (data.isSelected) {
                viewModel.selectMedicineNotify(data.id)
                scheduleOneTimeRequest(
                    context = requireContext(),
                    initialDelay = data.timeDiffMedInSeconds
                )
                Toast.makeText(
                    context,
                    "App Will Notify You at " + data.timeOfMed,
                    Toast.LENGTH_LONG
                ).show()
            } else {
                callTasks(requireContext())
            }
            remindersAdapter?.notifyDataSetChanged()
        }
    }

    private fun setUpViews() {

        viewModel.setToolbarVisibility(true)
    }

    private fun addDummyData() {
        val medicineTake1 = MedicineTake(
            id = 1,
            medicineDescription = "1 panadol , 4 disperin",
            daySlot = DaySlot.MORNING,
            timeOfMed = "08:00 AM",
            isSelected = false
        )
        viewModel.addifNotMedicineReminder(medicineTake1)
        //////////////////////////////////////////////////////////////////////////
        val medicineTake2 = MedicineTake(
            id = 2,
            medicineDescription = "1 panadol , 4 disperin",
            daySlot = DaySlot.AFTERNOON,
            timeOfMed = "1:00 PM",
            isSelected = false
        )
        viewModel.addifNotMedicineReminder(medicineTake2)
        //////////////////////////////////////////////////////////////////////////

        val medicineTake3 = MedicineTake(
            id = 3,
            medicineDescription = "1 panadol , 4 disperin",
            daySlot = DaySlot.EVENING,
            timeOfMed = "6:00 PM",
            isSelected = false
        )
        viewModel.addifNotMedicineReminder(medicineTake3)
        //////////////////////////////////////////////////////////////////////////
        val medicineTake4 = MedicineTake(
            id = 4,
            medicineDescription = "1 panadol , 4 disperin",
            daySlot = DaySlot.NIGHT,
            timeOfMed = "10:00 PM",
            isSelected = false
        )
        viewModel.addifNotMedicineReminder(medicineTake4)
        //////////////////////////////////////////////////////////////////////////
        // TODO remove below just for testing
        val medicineTake5 = MedicineTake(
            id = 5,
            medicineDescription = "test notification comes every 60 seconds",
            daySlot = DaySlot.UNKNOWN,
            timeOfMed = "",
            isSelected = false
        )
        viewModel.addifNotMedicineReminder(medicineTake5)
        //////////////////////////////////////////////////////////////////////////
    }

}