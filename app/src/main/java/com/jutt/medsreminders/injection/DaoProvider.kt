package com.jutt.medsreminders.injection

import com.jutt.medsreminders.data.models.MedicineTake
import com.jutt.medsreminders.data.persistence.AppDatabase
import com.jutt.medsreminders.data.persistence.daos.MedicineRemindersDao
import dagger.Module
import dagger.Provides

@Module
class DaoProvider {

    @Provides
    fun provideMedicineReminderDao(database: AppDatabase): MedicineRemindersDao = database.medicineReminderDao()

}
