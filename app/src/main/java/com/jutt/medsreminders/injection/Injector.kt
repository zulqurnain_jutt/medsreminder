package com.jutt.medsreminders.injection

import com.jutt.medsreminders.application.MyApp
import com.jutt.medsreminders.data.repositories.*
import com.jutt.medsreminders.helper.EventTracker
import com.jutt.medsreminders.helper.PushNotificationManager
import com.jutt.medsreminders.viewmodels.*

class Injector(application: MyApp) {

    object Named {
        const val REPOSITORY_DISPATCHER: String = "REPOSITORY_DISPATCHER"
        const val LOGGING_ENABLED: String = "LOGGING_ENABLED"
    }

    private val appComponent: AppComponent = DaggerAppComponent.builder()
        .daggerProvider(DaggerProvider(application = application))
        .build()

    @Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
    fun <T> provide(modelClass: Class<T>): T =
        with(modelClass) {
            return@with when {
                isAssignableFrom(PushNotificationManager::class.java) -> appComponent.pushNotificationManager()
                isAssignableFrom(DatabaseRepository::class.java) -> appComponent.databaseRepository()
                isAssignableFrom(HomeViewModel::class.java) -> appComponent.homeViewmodel()
                isAssignableFrom(EventTracker::class.java) -> appComponent.eventTracker()
                else -> throw IllegalArgumentException("Unknown class: ${modelClass.name}")
            } as T
        }
}
