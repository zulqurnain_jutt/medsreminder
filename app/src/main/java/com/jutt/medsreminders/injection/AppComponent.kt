package com.jutt.medsreminders.injection

import com.jutt.medsreminders.helper.EventTracker
import com.jutt.medsreminders.data.repositories.*
import com.jutt.medsreminders.helper.PushNotificationManager
import com.jutt.medsreminders.viewmodels.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        DaggerProvider::class,
        DaoProvider::class
    ]
)
interface AppComponent {

    fun pushNotificationManager(): PushNotificationManager
    fun databaseRepository(): DatabaseRepository
    fun homeViewmodel(): HomeViewModel
    fun eventTracker(): EventTracker

    @Component.Builder
    interface Builder {
        fun daggerProvider(provider: DaggerProvider): Builder
        fun build(): AppComponent
    }

}