package com.jutt.medsreminders.injection

import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.jutt.medsreminders.BuildConfig
import com.jutt.medsreminders.application.MyApp
import com.jutt.medsreminders.data.persistence.AppDatabase
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Named
import javax.inject.Singleton

@Module
class DaggerProvider(val application: MyApp) {

    @Provides
    fun provideContext(): Context = application.applicationContext

    @Provides
    @Named(Injector.Named.REPOSITORY_DISPATCHER)
    fun provideRepositoryDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Named(Injector.Named.LOGGING_ENABLED)
    fun provideLoggingEnabled(): Boolean = !BuildConfig.DEBUG

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase =
        AppDatabase.buildDatabase(context = context.applicationContext)

    @Provides
    @Singleton
    fun provideAnalytics(context: Context): FirebaseAnalytics =
        FirebaseAnalytics.getInstance(context)
}
